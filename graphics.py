import pyglet


class ActiveElement:
    def __init__(self, x_coord, y_coord, height=0, width=0):
        self.x_coord = x_coord
        self.y_coord = y_coord
        self.height = height
        self.width = width

        self.left_x = self.x_coord
        self.right_x = self.x_coord + self.width
        self.upper_y = self.y_coord + self.height
        self.lower_y = self.y_coord

    def is_touched(self, x_coord, y_coord):
        if self.left_x <= x_coord <= self.right_x and self.lower_y <= y_coord <= self.upper_y:
            return True
        else:
            return False


class Graphics:
    def __init__(self, mech):
        self.original_window_width = 800  # 640
        self.original_window_height = 600  # 480
        self.window = pyglet.window.Window(caption='Matesha', width=self.original_window_width,
                                           height=self.original_window_height, resizable=True)
        self.window.set_minimum_size(width=self.original_window_width, height=self.original_window_height)
        self.previous_window_sizes = (self.window.width, self.window.height)
        self.window_scale_flag = True

        self.scale_factor_x = self.window.width / self.original_window_width
        self.scale_factor_y = self.window.height / self.original_window_height
        self.scale_factor = min(self.scale_factor_x, self.scale_factor_y)
        self.x_shift = None
        self.y_shift = None

        # Характиристики полей
        self.x_field_shift_default = 1415 / 2  # левый нижний край игрового поля в пикселях
        self.y_field_shift_default = 673 / 2
        self.x_first_answer_shift_default = 900 / 2  # левый нижний край пустого поля ответа
        self.y_first_answer_shift_default = 420 / 2
        self.x_second_answer_shift_default = 1559 / 2  # левый нижний край заполненного поля ответа
        self.y_second_answer_shift_default = 207 / 2
        self.field_element_size_default = 110 / 2
        self.constant_field_element_size = self.field_element_size_default
        self.x_field_shift = None
        self.y_field_shift = None
        self.x_first_answer_shift = None
        self.y_first_answer_shift = None
        self.x_second_answer_shift = None
        self.y_second_answer_shift = None
        self.field_element_size = None
        self.field_number_elements = 10
        self.first_answer_number_elements = 19
        self.second_answer_number_elements = 7

        # Характеристики кнопочек
        self.buttons_height_def = 75 / 2
        self.buttons_width_new_game_def, self.buttons_width_pause_def = 530 / 2, 315 / 2
        self.buttons_width_credits_def, self.buttons_width_exit_def = 415 / 2, 210 / 2
        self.help_button_width_def = 124
        self.x_button_new_game_def, self.y_button_new_game_def = 500 / 2, 1480 / 2
        self.x_button_pause_def, self.y_button_pause_def = 500 / 2, 1280 / 2
        self.x_button_credits_def, self.y_button_credits_def = 500 / 2, 1080 / 2
        self.help_button_x_def, self.help_button_y_def = 500 / 2, 450
        self.x_button_exit_def, self.y_button_exit_def = 500 / 2, 360
        self.buttons_height = None
        self.buttons_width_new_game, self.buttons_width_pause = None, None
        self.buttons_width_credits, self.buttons_width_exit = None, None
        self.help_button_width = None
        self.x_button_new_game, self.y_button_new_game = None, None
        self.x_button_pause, self.y_button_pause = None, None
        self.x_button_credits, self.y_button_credits = None, None
        self.help_button_x, self.help_button_y = None, None
        self.x_button_exit, self.y_button_exit = None, None

        # Характеристики символов уровней, очков и пр.
        self.shift_before_symbol_starts_x = 24 / 2  # pixel
        self.shift_before_symbol_starts_y = 17 / 2
        self.shift_between_symbols = 73 / 2
        self.label_score_x_default = 2907 / 2
        self.label_speed_x_default = 2264 / 2
        self.label_level_x_default = 1682 / 2
        self.label_y_default = 1985 / 2
        self.label_score_x = None
        self.label_speed_x = None
        self.label_level_x = None
        self.label_y = None

        # Характеристики выбора скорости
        self.change_speed_x_default = 1235 / 2
        self.change_speed_y_default = 1456 / 2
        self.change_level_x_default = 1090 / 2
        self.change_level_y_default = 1456 / 2
        self.change_param_width_default = 77 / 2
        self.change_param_height_default = 135 / 2
        self.change_speed_x = None
        self.change_speed_y = None
        self.change_level_x = None
        self.change_level_y = None
        self.change_param_width = None
        self.change_param_height = None
        self.big_score_pic_height_default = None
        self.big_score_pic_width_default = None
        self.big_score_pic_width = None
        self.big_score_pic_height = None
        self.big_card_coef = 1.5

        # Характеристики карточек
        self.card_pic_width_default = None
        self.card_pic_height_default = None
        self.score_pic_width_default = None
        self.card_pic_width = None
        self.card_pic_height = None
        self.score_pic_width = None

        # Характеристики кнопок меню авторов
        self.credits_back_x_def, self.credits_back_y_def = 862, 15
        self.credits_back_x, self.credits_back_y = None, None
        self.credits_back_width_def, self.credits_back_height_def = 155, 49
        self.credits_back_width, self.credits_back_height = None, None

        # Характеристики кнопок меню выбора
        self.dialog_height_def, self.dialog_width_def = 86, 180
        self.dialog_height, self.dialog_width = None, None
        self.dialog_yes_x_def, self.dialog_yes_y_def = 660, 415
        self.dialog_yes_x, self.dialog_yes_y = None, None
        self.dialog_no_x_def, self.dialog_no_y_def = 1082, 415
        self.dialog_no_x, self.dialog_no_y = None, None

        # Характеристики слайдеров музыки
        self.slider_height_def, self.slider_width_def = 60, 50
        self.slider_height, self.slider_width = None, None
        self.slider_first_x_def, self.slider_first_y_def = 1452, 439
        self.slider_first_x, self.slider_first_y = None, None
        self.slider_second_x_def, self.slider_second_y_def = 1627, 366
        self.slider_second_x, self.slider_second_y = None, None

        # Характеристики окна трейд ина front_adv
        self.trade_x_def, self.trade_y_def = 1445, 775
        self.trade_x, self.trade_y = None, None
        self.trade_height_def, self.trade_width_def = 59, 59
        self.trade_height, self.trade_width = None, None

        # Характеристики ускорения падения
        self.add_extra_card_x_def, self.add_extra_card_y_def = 1405, 585  # WILL BE CHANGED
        self.add_extra_card_x, self.add_extra_card_y = None, None
        self.add_extra_card_height_def, self.add_extra_card_width_def = 27, 129  # WILL BE CHANGED
        self.add_extra_card_height, self.add_extra_card_width = None, None

        self.main_background_scale_factor = None
        self.backgrounds_list = [None]  # 0 - пустой элемент, чтобы  было понятно, 1 - левый верхний, 9 - правый нижний
        self.mech = mech

        # Громкость музыки
        self.music_volume, self.sounds_volume = self.mech.get_volumes()
        self.show_sliders = True

        self.new_images_path = 'sprites/resized/'
        self.score_symbols_path = self.new_images_path + 'score symbols/'
        self.symbols_path = self.new_images_path + 'symbols/'
        self.window.set_icon(pyglet.image.load(self.new_images_path + 'icon.png'))

        pyglet.gl.glClearColor(1, 1, 1, 1)
        self.batch = pyglet.graphics.Batch()
        self.background = pyglet.graphics.OrderedGroup(0)
        self.foreground = pyglet.graphics.OrderedGroup(1)
        self.card_layer = pyglet.graphics.OrderedGroup(3)
        self.empty_card_layer = pyglet.graphics.OrderedGroup(4)
        self.drag_card_layer = pyglet.graphics.OrderedGroup(5)
        self.dialog_layer = pyglet.graphics.OrderedGroup(7)

        # Активные игровые элементы
        # кнопок в меню
        self.menu_start_game = None
        self.menu_options = None
        self.menu_quit = None
        self.menu_pause = None
        self.menu_help = None

        # игровых полей
        self.playing_background_element = None
        self.expression_field_element = None
        self.score_field_element = None

        # изменения режима игры
        self.change_speed_element = None
        self.change_level_element = None

        # меню авторов и разрешения на действия
        self.credits_back = None
        self.dialog_yes = None
        self.dialog_no = None

        self.list_of_answer_cards = []
        self.list_of_bord_cards = []
        self.list_of_expression_cards = []

        self.credits_sprite = None
        self.dialog_sprite = None
        self.help_sprite = None

        # слайдеров
        self.slider_area_1 = None
        self.slider_area_2 = None
        self.slider_sprite_1 = None
        self.slider_sprite_2 = None

        # обмена и ускорения
        self.trade = None
        self.add_extra_card = None

        # MAIN BACKGROUND
        self.main_background = pyglet.sprite.Sprite(pyglet.image.load(self.new_images_path + 'front.png'),
                                                    batch=self.batch,
                                                    group=self.foreground)
        self.main_background_scale_factor = min(self.window.height / self.main_background.height,
                                                self.window.width / self.main_background.width)
        self.main_background.scale = self.main_background_scale_factor

        # 8 BACKGROUNDS
        for i in range(1, 10):
            self.additional_background = pyglet.sprite.Sprite(
                pyglet.image.load(self.new_images_path + 'background.jpg'),
                batch=self.batch,
                group=self.background)
            self.additional_background.scale = self.main_background_scale_factor
            self.backgrounds_list.append(self.additional_background)

        # хранилище спрайтов карточек, которые мы когда-либо создавали.
        # его структура такая: это словарь {'plus': [спрайт], '1': [спрайт, спрайт], '2': [спрайт, спрайт], ...}.
        # то есть на каждом цикле отрисовки не удаляются старые спрайты и создаются новые.
        # когда мы хотим отрисовать карточку, смотрим, есть ли незанятый спрайт в хранилище- если есть,
        # то делаем sprite = self.card_sprite_storage['1'].pop(), если нет, то создаем совсем новый.
        # далее делаем этому спрайту правильные координаты, выставляем visible и перекладываем в self.card_sprites,
        # который имеет ту же структуру. в кард_спрайтс лежат спрайты, которые нужно отрисовать.
        # на каждом цикле отрисовки, спрайты из кард_спрайтс перекладываем на свои места в кард_спрайт_сторадж,
        # выставив им невидимость

        self.empty_picture = pyglet.image.load(self.symbols_path + 'empty.png')
        self.empty_card_storage = []
        self.empty_card_sprites = []

        self.card_sprite_storage = {}
        self.card_sprites = {}
        self.card_pictures = {}

        # ниже создается пустое хранилище свободных спрайтов и хранилище спрайтов,
        # которые надо отрисовать. а также подгружаются все картинки
        self.list_cards_symbols = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                                   'plus', 'minus', 'multiply', 'divide', 'equally',
                                   'open_bkt', 'close_bkt', 'empty')
        for card_value in self.list_cards_symbols:
            self.card_pictures[card_value] = pyglet.image.load(self.symbols_path + '{}.png'.format(card_value))

            self.card_sprite_storage[card_value] = []
            self.card_sprites[card_value] = []

        self.equally_pic = pyglet.image.load(self.symbols_path + 'equally.png')

        self.score_sprite_storage = {}
        self.score_sprites = {}
        self.score_pictures = {}

        self.list_score_symbols = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
        for score_symbol in self.list_score_symbols:
            self.score_pictures[score_symbol] = pyglet.image.load(self.score_symbols_path +
                                                                  '{}.png'.format(score_symbol))
            self.score_sprite_storage[score_symbol] = []
            self.score_sprites[score_symbol] = []

        self.big_score_sprite_storage = {}
        self.big_score_sprites = {}
        self.big_score_pictures = {}
        for score_symbol in self.list_score_symbols:
            self.big_score_pictures[score_symbol] = pyglet.image.load(self.score_symbols_path +
                                                                      '{}.png'.format(score_symbol))
            self.big_score_sprite_storage[score_symbol] = []
            self.big_score_sprites[score_symbol] = []

        self.default_preupdate_factors(self.main_background.scale)  # HERE PREUPDATE

    def get_window(self):
        return self.window

    # Shows different menus
    def show_menu(self, dialog=False):
        for key in self.card_sprites:
            for item in self.card_sprites[key]:
                item.visible = False
        for key in self.score_sprites:
            for item in self.score_sprites[key]:
                item.visible = False
        for key in self.big_score_sprites:
            for item in self.big_score_sprites[key]:
                item.visible = False
        for item in self.empty_card_sprites:
            item.visible = False
        # self.update_change_parameters()
        self.draw_change_speed(str(self.mech.start_speed))
        self.draw_change_level(str(self.mech.start_level))

        self.main_background.visible = True
        self.credits_sprite.visible = False
        self.help_sprite.visible = False
        self.dialog_sprite.visible = dialog

    def show_options(self, help=False):
        for key in self.card_sprites:
            for item in self.card_sprites[key]:
                item.visible = False
        for key in self.score_sprites:
            for item in self.score_sprites[key]:
                item.visible = False
        for key in self.big_score_sprites:
            for item in self.big_score_sprites[key]:
                item.visible = False
        for item in self.empty_card_sprites:
            item.visible = False
        self.main_background.visible = False
        self.credits_sprite.visible = True
        self.dialog_sprite.visible = False
        if help:
            self.help_sprite.visible = True
            self.credits_sprite.visible = False
        else:
            self.credits_sprite.visible = True
            self.help_sprite.visible = False

    def show_dialog(self):
        for key in self.card_sprites:
            for item in self.card_sprites[key]:
                item.visible = False
        for key in self.score_sprites:
            for item in self.score_sprites[key]:
                item.visible = False
        for key in self.big_score_sprites:
            for item in self.big_score_sprites[key]:
                item.visible = False
        for item in self.empty_card_sprites:
            item.visible = False

        self.main_background.visible = True
        self.credits_sprite.visible = False
        self.help_sprite.visible = False
        self.dialog_sprite.visible = True

    def show_game(self, dialog=False, paused=False):
        field, width, height = self.mech.get_playfield()  # поле из механики, в котором лежат карточки для их отрисовки

        # REFRESH SPRITES(CARDS, SCORES, BIG_SPRITES, EMPTY)
        for key in self.card_sprites:
            while len(self.card_sprites[key]) != 0:
                # вынимаем каждый спрайт из кард_спрайтс, которые мы туда положили на предыдущем витке цикла отрисовки
                # делаем его невидимыми и перекладываем в нужное место хранилища с свободными спрайтами
                item = self.card_sprites[key].pop()
                item.visible = False
                self.card_sprite_storage[key].append(item)
        for key in self.score_sprites:
            while(len(self.score_sprites[key])) != 0:
                item = self.score_sprites[key].pop()
                item.visible = False
                self.score_sprite_storage[key].append(item)
        for key in self.big_score_sprites:
            while(len(self.big_score_sprites[key])) != 0:
                item = self.big_score_sprites[key].pop()
                item.visible = False
                self.big_score_sprite_storage[key].append(item)
        while(len(self.empty_card_sprites)) != 0:
            item = self.empty_card_sprites.pop()
            item.visible = False
            self.empty_card_storage.append(item)

        # здесь проходимся по всему полю с карточками, если карточка в этом месте есть, то отрисовываем ее
        # отрисовываем путем взятия карточки из кард_спрайт_стораже,
        # приписывания нужных координат, визибле=тру и перекладывании из кард_спрайт_стораже в кард_спрайтс
        for i in range(height):
            for j in range(width):
                card = field.get_card_by_indexes(i, j)
                if card is None:
                    continue
                self.draw_card_on_field(card, i, j, paused=paused)
        # дальше здесь надо запросить из механики содержимое строчки формулы
        formulafield, expr_length, answer_length = self.mech.get_formulafield()
        for i in range(expr_length):
            card = formulafield.get_card_expression_by_index(i)
            if card is None:
                continue
            self.draw_card_on_expression_formula(card, i, paused=paused)
            # надо будет это написать, куда рисовать карточку, зная ее место в формуле,
            # работает так же как и draw_on_field

        for i in range(answer_length):
            card = formulafield.get_card_answer_by_index(i)
            if card is None:
                continue
            self.draw_card_on_answer_formula(card, i, paused=paused)
            # надо будет это написать, куда рисовать карточку,зная ее место в формуле

        # CARRY CARD ACTIONS
        carried_card = self.mech.get_carried_card()
        if carried_card is not None:
            card_value = carried_card.card_value
            x_start = carried_card.card_x
            y_start = carried_card.card_y
            if len(self.card_sprite_storage[card_value]) != 0:
                sprite = self.card_sprite_storage[card_value].pop()
                sprite.x = x_start - sprite.width//2
                sprite.y = y_start - sprite.height//2
                sprite.visible = True
                sprite.group = self.drag_card_layer
                self.card_sprites[card_value].append(sprite)
            else:
                new_sprite = pyglet.sprite.Sprite(self.card_pictures[card_value],
                                                  batch=self.batch,
                                                  group=self.drag_card_layer)

                new_sprite.x = x_start - new_sprite.width//2
                new_sprite.y = y_start - new_sprite.height//2
                new_sprite.visible = True
                self.card_sprites[card_value].append(new_sprite)

        if paused:
            self.draw_empties_on_answer_formula()

        self.main_background.visible = True
        self.credits_sprite.visible = False
        self.help_sprite.visible = False
        self.dialog_sprite.visible = dialog

        self.draw_score(self.mech.get_score())

        self.draw_speed(str(self.mech.get_speed()))

        self.draw_level(str(self.mech.get_level()))

        # Change speed and level
        self.draw_change_speed(str(self.mech.start_speed))

        self.draw_change_level(str(self.mech.start_level))

    # draw score and speed and level
    def draw_score(self, score):
        str_score = str(score)
        str_score_reversed = str_score[::-1]
        for i in range(len(str_score_reversed)):
            score_symbol = str_score_reversed[i]
            score_index = 6 - i
            self.draw_score_symbols(score_symbol, score_index)

    def draw_score_symbols(self, symbol, index):
        x_start = self.x_shift + self.label_score_x - self.shift_before_symbol_starts_x * self.main_background.scale + \
                  (index - 1) * (self.shift_between_symbols * self.main_background.scale)
        y_start = (self.y_shift + self.label_y) - self.shift_before_symbol_starts_y * self.main_background.scale
        if len(self.score_sprite_storage[symbol]) != 0:
            sprite = self.score_sprite_storage[symbol].pop()
            sprite.x = x_start
            sprite.y = y_start
            sprite.visible = True
            self.score_sprites[symbol].append(sprite)
        else:
            new_sprite = pyglet.sprite.Sprite(self.score_pictures[symbol],
                                              x=x_start,
                                              y=y_start,
                                              batch=self.batch,
                                              group=self.card_layer)
            new_sprite.visible = True
            self.score_sprites[symbol].append(new_sprite)

    def draw_speed(self, speed):
        x_start = self.x_shift + self.label_speed_x - self.shift_before_symbol_starts_x * self.main_background.scale
        y_start = self.y_shift + self.label_y - self.shift_before_symbol_starts_y * self.main_background.scale
        if len(self.score_sprite_storage[speed]) != 0:
            sprite = self.score_sprite_storage[speed].pop()
            sprite.x = x_start
            sprite.y = y_start
            sprite.visible = True
            self.score_sprites[speed].append(sprite)
        else:
            new_sprite = pyglet.sprite.Sprite(self.score_pictures[speed],
                                              x=x_start,
                                              y=y_start,
                                              batch=self.batch,
                                              group=self.card_layer)
            new_sprite.visible = True
            self.score_sprites[speed].append(new_sprite)

    def draw_level(self, level):
        x_start = self.x_shift + self.label_level_x - self.shift_before_symbol_starts_x * self.main_background.scale
        y_start = self.y_shift + self.label_y - self.shift_before_symbol_starts_y * self.main_background.scale
        if len(self.score_sprite_storage[level]) != 0:
            sprite = self.score_sprite_storage[level].pop()
            sprite.x = x_start
            sprite.y = y_start
            sprite.visible = True
            self.score_sprites[level].append(sprite)
        else:
            new_sprite = pyglet.sprite.Sprite(self.score_pictures[level],
                                              x=x_start,
                                              y=y_start,
                                              batch=self.batch,
                                              group=self.card_layer)
            new_sprite.visible = True
            self.score_sprites[level].append(new_sprite)

    def draw_change_speed(self, speed):
        shift_card_coef = self.main_background.scale * self.big_card_coef
        x_start = self.x_shift + self.change_speed_x - self.shift_before_symbol_starts_x * shift_card_coef
        y_start = self.y_shift + self.change_speed_y - self.shift_before_symbol_starts_y * shift_card_coef
        if len(self.big_score_sprite_storage[speed]) != 0:
            sprite = self.big_score_sprite_storage[speed].pop()
            sprite.x = x_start
            sprite.y = y_start
            sprite.visible = True
            self.big_score_sprites[speed].append(sprite)
        else:
            new_sprite = pyglet.sprite.Sprite(self.big_score_pictures[speed],
                                              x=x_start,
                                              y=y_start,
                                              batch=self.batch,
                                              group=self.card_layer)
            new_sprite.visible = True
            self.big_score_sprites[speed].append(new_sprite)

    def draw_change_level(self, level):
        shift_card_coef = self.main_background.scale * self.big_card_coef
        x_start = self.x_shift + self.change_level_x - self.shift_before_symbol_starts_x * shift_card_coef
        y_start = self.y_shift + self.change_level_y - self.shift_before_symbol_starts_y * shift_card_coef
        if len(self.big_score_sprite_storage[level]) != 0:
            sprite = self.big_score_sprite_storage[level].pop()
            sprite.x = x_start
            sprite.y = y_start
            sprite.visible = True
            self.big_score_sprites[level].append(sprite)
        else:
            new_sprite = pyglet.sprite.Sprite(self.big_score_pictures[level],
                                              x=x_start,
                                              y=y_start,
                                              batch=self.batch,
                                              group=self.card_layer)
            new_sprite.visible = True
            self.big_score_sprites[level].append(new_sprite)

    # Draw cards on fields
    def draw_card_on_expression_formula(self, card, index, paused=False):
        offset = 0.5 * self.main_background.scale
        expression_offset_x = (self.x_shift + self.x_first_answer_shift)
        expression_offset_y = (self.y_shift + self.y_first_answer_shift)
        x_start = (self.card_pic_width + offset) * index + offset + expression_offset_x
        y_start = expression_offset_y + offset
        card_value = card.card_value
        if len(self.card_sprite_storage[card_value]) != 0:
            sprite = self.card_sprite_storage[card_value].pop()
            sprite.x = x_start
            sprite.y = y_start
            sprite.visible = not paused
            sprite.group = self.card_layer
            self.card_sprites[card_value].append(sprite)

            if len(self.empty_card_storage) != 0:
                empty = self.empty_card_storage.pop()
                empty.x = x_start
                empty.y = y_start
                empty.visible = paused
                self.empty_card_sprites.append(empty)
        else:
            new_sprite = pyglet.sprite.Sprite(self.card_pictures[card_value],
                                              x=x_start,
                                              y=y_start,
                                              batch=self.batch,
                                              group=self.card_layer)
            new_sprite.visible = not paused
            self.card_sprites[card_value].append(new_sprite)

            empty_sprite = pyglet.sprite.Sprite(self.card_pictures['empty'],
                                                x=x_start,
                                                y=y_start,
                                                batch=self.batch,
                                                group=self.empty_card_layer)
            empty_sprite.visible = paused
            empty_sprite.scale = self.main_background.scale
            self.empty_card_sprites.append(empty_sprite)

    def draw_card_on_answer_formula(self, card, index, paused=False):
        offset = 1 * self.main_background.scale
        answer_offset_x = (self.x_shift + self.x_second_answer_shift)
        x_start = (self.card_pic_width + offset) * index + offset + answer_offset_x
        y_start = (self.y_shift + self.y_second_answer_shift) + offset
        card_value = card.card_value
        if len(self.card_sprite_storage[card_value]) != 0:
            sprite = self.card_sprite_storage[card_value].pop()
            sprite.x = x_start
            sprite.y = y_start
            sprite.visible = not paused or index == 0
            sprite.group = self.card_layer
            self.card_sprites[card_value].append(sprite)
        else:
            new_sprite = pyglet.sprite.Sprite(self.card_pictures[card_value],
                                              x=x_start,
                                              y=y_start,
                                              batch=self.batch,
                                              group=self.card_layer)

            new_sprite.scale = self.main_background.scale  # Нужно обновлять размер при скейле
            new_sprite.visible = not paused or index == 0
            self.card_sprites[card_value].append(new_sprite)

    def draw_card_on_field(self, card, x_ind, y_ind, paused=False):
        # тута рисуем карточку, зная ее координаты в поле
        offset = 1 * self.main_background.scale
        field_offset_x = (self.x_shift + self.x_field_shift)
        field_offset_y = (self.y_shift + self.y_field_shift)
        x_start = (self.card_pic_width + offset) * x_ind + offset + field_offset_x
        y_start = (self.card_pic_height + offset) * y_ind + offset + field_offset_y
        card_value = card.card_value
        # смотрим, не пусто ли в хранилище свободных спрайтов
        if len(self.card_sprite_storage[card_value]) != 0:
            # если не пусто, то берем последний спрайт оттуда
            sprite = self.card_sprite_storage[card_value].pop()
            sprite.x = x_start
            sprite.y = y_start
            sprite.visible = not paused
            sprite.group = self.card_layer
            # приписали нужные координаты и визибле,
            # добавили в хранилище спрайтов, которые нужно отрисовать на этом витке цикла
            self.card_sprites[card_value].append(sprite)

            if len(self.empty_card_storage) != 0:
                empty = self.empty_card_storage.pop()
                empty.x = x_start
                empty.y = y_start
                empty.visible = paused
                self.empty_card_sprites.append(empty)
        else:
            # если свободного спрайта не имеется, то создаем новый, с правильной картинкой
            # и кладем в правильную ячейку хранилища спрайтов для отрисовки
            new_sprite = pyglet.sprite.Sprite(self.card_pictures[card_value],
                                              x=x_start,
                                              y=y_start,
                                              batch=self.batch,
                                              group=self.card_layer)

            new_sprite.scale = self.main_background.scale  # Нужно обновлять размер при скейле
            new_sprite.visible = not paused
            self.card_sprites[card_value].append(new_sprite)

            empty_sprite = pyglet.sprite.Sprite(self.card_pictures['empty'],
                                                x=x_start,
                                                y=y_start,
                                                batch=self.batch,
                                                group=self.empty_card_layer)
            empty_sprite.visible = paused
            empty_sprite.scale = self.main_background.scale
            self.empty_card_sprites.append(empty_sprite)

    def draw_empties_on_answer_formula(self):
        offset = 1 * self.main_background.scale
        answer_offset_x = (self.x_shift + self.x_second_answer_shift)
        y_start = (self.y_shift + self.y_second_answer_shift) + offset
        for index in range(1, 8):
            x_start = (self.card_pic_width + offset) * index + offset + answer_offset_x
            if len(self.empty_card_storage) != 0:
                empty = self.empty_card_storage.pop()
                empty.x = x_start
                empty.y = y_start
                empty.visible = True
                self.empty_card_sprites.append(empty)
            else:
                empty_sprite = pyglet.sprite.Sprite(self.card_pictures['empty'],
                                                    x=x_start,
                                                    y=y_start,
                                                    batch=self.batch,
                                                    group=self.empty_card_layer)
                empty_sprite.visible = True
                empty_sprite.scale = self.main_background.scale
                self.empty_card_sprites.append(empty_sprite)

    def get_playing_field_index(self, x, y):
        i_card_index = int((x - self.x_shift - self.x_field_shift) /
                           self.main_background.scale // self.constant_field_element_size)
        j_card_index = int((y - self.y_shift - self.y_field_shift) /
                           self.main_background.scale // self.constant_field_element_size)
        return i_card_index, j_card_index

    def get_expression_field_index(self, x):
        i_card_index = int((x - self.x_shift - self.x_first_answer_shift) /
                           self.main_background.scale // self.constant_field_element_size)
        return i_card_index

    # Music events
    def set_volume(self, x, music=False, reset=False):
        volume = (x - self.slider_first_x - self.x_shift) / \
                  (self.slider_second_x - self.slider_first_x)
        volume = 0 if volume < 0.05 else volume
        if reset:
            volume = 0.5
        if music:
            self.mech.set_music_volume(volume)
            self.music_volume = volume

        else:
            self.mech.set_sounds_volume(volume)
            self.sounds_volume = volume

    # Main schedule functions
    def update_picture(self):
        self.window_scale_flag = self.is_window_scaled()
        if self.window_scale_flag:
            self.update_scaling_and_shifting()
            self.update_buttons()
            self.update_labels()
            self.update_fields()
            self.update_background()
            self.update_menus()
            self.update_active_game_features()

        self.update_change_parameters()
        self.update_cards()
        self.update_sliders()

        self.previous_window_sizes = (self.window.width, self.window.height)
        self.window.clear()
        self.batch.draw()

    def default_preupdate_factors(self, scale):
        # Main shifts
        self.x_shift = (self.window.width - self.main_background.width) // 2
        self.y_shift = (self.window.height - self.main_background.height) // 2

        # Fields
        self.x_field_shift_default = self.x_field_shift_default * scale
        self.y_field_shift_default = self.y_field_shift_default * scale
        self.x_first_answer_shift_default = self.x_first_answer_shift_default * scale
        self.y_first_answer_shift_default = self.y_first_answer_shift_default * scale
        self.x_second_answer_shift_default = self.x_second_answer_shift_default * scale
        self.y_second_answer_shift_default = self.y_second_answer_shift_default * scale
        self.field_element_size_default = self.field_element_size_default * scale

        # Buttons
        self.buttons_height_def = self.buttons_height_def * scale
        self.buttons_width_new_game_def = self.buttons_width_new_game_def * scale
        self.buttons_width_pause_def = self.buttons_width_pause_def * scale
        self.buttons_width_credits_def = self.buttons_width_credits_def * scale
        self.buttons_width_exit_def = self.buttons_width_exit_def * scale
        self.help_button_width_def = self.help_button_width_def * scale
        self.x_button_new_game_def = self.x_button_new_game_def * scale
        self.y_button_new_game_def = self.y_button_new_game_def * scale
        self.x_button_pause_def = self.x_button_pause_def * scale
        self.y_button_pause_def = self.y_button_pause_def * scale
        self.x_button_credits_def = self.x_button_credits_def * scale
        self.y_button_credits_def = self.y_button_credits_def * scale
        self.x_button_exit_def = self.x_button_exit_def * scale
        self.y_button_exit_def = self.y_button_exit_def * scale
        self.help_button_x_def = self.help_button_x_def * scale
        self.help_button_y_def = self.help_button_y_def * scale

        self.menu_start_game = ActiveElement(self.x_button_new_game_def + self.x_shift,
                                             self.y_button_new_game_def + self.y_shift,
                                             self.buttons_height_def, self.buttons_width_new_game_def)
        self.menu_pause = ActiveElement(self.x_button_pause_def + self.x_shift,
                                        self.y_button_pause_def + self.y_shift,
                                        self.buttons_height_def, self.buttons_width_pause_def)
        self.menu_options = ActiveElement(self.x_button_credits_def + self.x_shift,
                                          self.y_button_credits_def + self.y_shift,
                                          self.buttons_height_def, self.buttons_width_credits_def)
        self.menu_quit = ActiveElement(self.x_button_exit_def + self.x_shift,
                                       self.y_button_exit_def + self.y_shift,
                                       self.buttons_height_def, self.buttons_width_exit_def)
        self.menu_help = ActiveElement(self.help_button_x_def + self.x_shift,
                                       self.help_button_y_def + self.y_shift,
                                       self.buttons_height_def, self.help_button_width_def)

        # Labels
        self.label_score_x_default = self.label_score_x_default
        self.label_speed_x_default = self.label_speed_x_default
        self.label_level_x_default = self.label_level_x_default
        self.label_y_default = self.label_y_default

        # Cards
        self.card_pic_width_default = self.card_pictures['0'].width
        self.card_pic_height_default = self.card_pictures['0'].width
        self.score_pic_width_default = self.score_pictures['0'].height

        # Changes
        self.change_speed_x_default = self.change_speed_x_default
        self.change_speed_y_default = self.change_speed_y_default
        self.change_level_x_default = self.change_level_x_default
        self.change_level_y_default = self.change_level_y_default
        self.change_param_width_default = self.change_param_width_default
        self.change_param_height_default = self.change_param_height_default
        self.big_score_pic_height_default = self.big_score_pictures['0'].height
        self.big_score_pic_width_default = self.big_score_pictures['0'].width

        # Credits and dialog
        self.credits_back_x_def = self.credits_back_x_def * scale
        self.credits_back_y_def = self.credits_back_y_def * scale
        self.credits_back_height_def = self.credits_back_height_def * scale
        self.credits_back_width_def = self.credits_back_width_def * scale
        self.dialog_height_def = self.dialog_height_def * scale
        self.dialog_width_def = self.dialog_width_def * scale
        self.dialog_yes_x_def = self.dialog_yes_x_def * scale
        self.dialog_yes_y_def = self.dialog_yes_y_def * scale
        self.dialog_no_x_def = self.dialog_no_x_def * scale
        self.dialog_no_y_def = self.dialog_no_y_def * scale

        self.credits_sprite = pyglet.sprite.Sprite(pyglet.image.load(self.new_images_path + 'credits.png'),
                                                   x=self.main_background.x,
                                                   y=self.main_background.y,
                                                   batch=self.batch,
                                                   group=self.foreground)
        self.credits_sprite.visible = False
        self.credits_sprite.scale = self.main_background.scale

        self.credits_back = ActiveElement(self.credits_back_x_def + self.x_shift,
                                          self.credits_back_y_def + self.y_shift,
                                          self.credits_back_height_def, self.credits_back_width_def)

        self.dialog_sprite = pyglet.sprite.Sprite(pyglet.image.load(self.new_images_path + 'big_dialog.png'),
                                                  x=self.main_background.x,
                                                  y=self.main_background.y,
                                                  batch=self.batch,
                                                  group=self.dialog_layer)
        self.dialog_sprite.visible = False
        self.dialog_sprite.scale = self.main_background.scale

        self.help_sprite = pyglet.sprite.Sprite(pyglet.image.load(self.new_images_path + 'help.png'),
                                                x=self.main_background.x,
                                                y=self.main_background.y,
                                                batch=self.batch,
                                                group=self.foreground)
        self.help_sprite.visible = False
        self.help_sprite.scale = self.main_background.scale

        self.dialog_yes = ActiveElement(self.dialog_yes_x_def + self.x_shift,
                                        self.dialog_yes_y_def + self.y_shift,
                                        self.dialog_height_def, self.dialog_width_def)
        self.dialog_no = ActiveElement(self.dialog_no_x_def + self.x_shift,
                                       self.dialog_no_y_def + self.y_shift,
                                       self.dialog_height_def, self.dialog_width_def)

        # Sliders
        self.slider_height_def = self.slider_height_def * scale
        self.slider_width_def = self.slider_width_def * scale
        self.slider_first_x_def = self.slider_first_x_def * scale
        self.slider_first_y_def = self.slider_first_y_def * scale
        self.slider_second_x_def = self.slider_second_x_def * scale
        self.slider_second_y_def = self.slider_second_y_def * scale

        self.slider_sprite_1 = pyglet.sprite.Sprite(pyglet.image.load(self.new_images_path + 'slider.png'),
                                                    x=(self.slider_second_x_def + self.slider_first_x_def) / 2 -
                                                    self.slider_width_def / 2 + self.x_shift,
                                                    y=self.slider_first_y_def - self.slider_height_def * 0.15 +
                                                    self.y_shift,
                                                    batch=self.batch,
                                                    group=self.dialog_layer)
        self.slider_sprite_1.scale = 0.5
        self.slider_area_1 = ActiveElement(self.slider_first_x_def + self.x_shift,
                                           self.slider_first_y_def + self.y_shift - self.slider_height_def / 5,
                                           self.slider_height_def / 1.5,
                                           self.slider_second_x_def - self.slider_first_x_def)

        self.slider_sprite_2 = pyglet.sprite.Sprite(pyglet.image.load(self.new_images_path + 'slider.png'),
                                                    x=(self.slider_second_x_def + self.slider_first_x_def) / 2 -
                                                    self.slider_width_def / 2 + self.x_shift,
                                                    y=self.slider_second_y_def - self.slider_height_def * 0.15 +
                                                    self.y_shift,
                                                    batch=self.batch,
                                                    group=self.dialog_layer)
        self.slider_sprite_2.scale = 0.5
        self.slider_area_2 = ActiveElement(self.slider_first_x_def + self.x_shift,
                                           self.slider_second_y_def + self.y_shift - self.slider_height_def / 5,
                                           self.slider_height_def / 1.5,
                                           self.slider_second_x_def - self.slider_first_x_def)

        # add_extra_card and trade in def
        self.trade_x_def = self.trade_x_def * scale
        self.trade_y_def = self.trade_y_def * scale
        self.trade_height_def = self.trade_height_def * scale
        self.trade_width_def = self.trade_width_def * scale
        self.add_extra_card_x_def = self.add_extra_card_x_def * scale
        self.add_extra_card_y_def = self.add_extra_card_y_def * scale
        self.add_extra_card_height_def = self.add_extra_card_height_def * scale
        self.add_extra_card_width_def = self.add_extra_card_width_def * scale

        self.trade = ActiveElement(self.trade_x_def + self.x_shift,
                                   self.trade_y_def + self.y_shift,
                                   self.trade_height_def, self.trade_width_def)

        self.add_extra_card = ActiveElement(self.add_extra_card_x_def + self.x_shift,
                                            self.add_extra_card_y_def + self.y_shift,
                                            self.add_extra_card_height_def, self.add_extra_card_width_def)

        # Update everything
        self.update_background()
        self.update_fields()
        self.update_labels()
        self.update_change_parameters()
        self.update_menus()

    def update_scaling_and_shifting(self):
        self.scale_factor_x = self.window.width / self.original_window_width
        self.scale_factor_y = self.window.height / self.original_window_height
        self.scale_factor = min(self.scale_factor_x, self.scale_factor_y)
        self.x_shift = (self.window.width - self.main_background.width) // 2
        self.y_shift = (self.window.height - self.main_background.height) // 2

        self.main_background.scale = self.main_background_scale_factor * self.scale_factor

    # Update game elements
    def update_background(self):
        for i in range(1, 10):
            self.backgrounds_list[i].scale = self.main_background.scale
        # 1
        self.backgrounds_list[1].x = self.x_shift - self.backgrounds_list[1].width
        self.backgrounds_list[1].y = self.y_shift + self.backgrounds_list[1].height
        # 2
        self.backgrounds_list[2].x = self.x_shift
        self.backgrounds_list[2].y = self.y_shift + self.backgrounds_list[2].height
        # 3
        self.backgrounds_list[3].x = self.x_shift + self.backgrounds_list[3].width
        self.backgrounds_list[3].y = self.y_shift + self.backgrounds_list[3].height
        # 4
        self.backgrounds_list[4].x = self.x_shift - self.backgrounds_list[4].width
        self.backgrounds_list[4].y = self.y_shift
        # 5 - main
        self.main_background.x = self.x_shift
        self.main_background.y = self.y_shift
        self.backgrounds_list[5].x = self.x_shift
        self.backgrounds_list[5].y = self.y_shift
        # 6
        self.backgrounds_list[6].x = self.x_shift + self.backgrounds_list[6].width
        self.backgrounds_list[6].y = self.y_shift
        # 7
        self.backgrounds_list[7].x = self.x_shift - self.backgrounds_list[7].width
        self.backgrounds_list[7].y = self.y_shift - self.backgrounds_list[7].height
        # 8
        self.backgrounds_list[8].x = self.x_shift
        self.backgrounds_list[8].y = self.y_shift - self.backgrounds_list[8].height
        # 9
        self.backgrounds_list[9].x = self.x_shift + self.backgrounds_list[9].width
        self.backgrounds_list[9].y = self.y_shift - self.backgrounds_list[9].height

    def update_buttons(self):
        self.buttons_height = self.buttons_height_def * self.scale_factor
        self.buttons_width_new_game = self.buttons_width_new_game_def * self.scale_factor
        self.buttons_width_pause = self.buttons_width_pause_def * self.scale_factor
        self.buttons_width_credits = self.buttons_width_credits_def * self.scale_factor
        self.buttons_width_exit = self.buttons_width_exit_def * self.scale_factor
        self.x_button_new_game = self.x_button_new_game_def * self.scale_factor
        self.y_button_new_game = self.y_button_new_game_def * self.scale_factor
        self.x_button_pause = self.x_button_pause_def * self.scale_factor
        self.y_button_pause = self.y_button_pause_def * self.scale_factor
        self.x_button_credits = self.x_button_credits_def * self.scale_factor
        self.y_button_credits = self.y_button_credits_def * self.scale_factor
        self.x_button_exit = self.x_button_exit_def * self.scale_factor
        self.y_button_exit = self.y_button_exit_def * self.scale_factor
        self.help_button_x = self.help_button_x_def * self.scale_factor
        self.help_button_y = self.help_button_y_def * self.scale_factor
        self.help_button_width = self.help_button_width_def * self.scale_factor

        self.menu_start_game = ActiveElement(self.x_button_new_game + self.x_shift,
                                             self.y_button_new_game + self.y_shift,
                                             self.buttons_height, self.buttons_width_new_game)
        self.menu_pause = ActiveElement(self.x_button_pause + self.x_shift,
                                        self.y_button_pause + self.y_shift,
                                        self.buttons_height, self.buttons_width_pause)
        self.menu_options = ActiveElement(self.x_button_credits + self.x_shift,
                                          self.y_button_credits + self.y_shift,
                                          self.buttons_height, self.buttons_width_credits)
        self.menu_quit = ActiveElement(self.x_button_exit + self.x_shift,
                                       self.y_button_exit + self.y_shift,
                                       self.buttons_height, self.buttons_width_exit)
        self.menu_help = ActiveElement(self.help_button_x + self.x_shift,
                                       self.help_button_y + self.y_shift,
                                       self.buttons_height, self.help_button_width)

    def update_fields(self):
        self.x_field_shift = self.x_field_shift_default * self.scale_factor
        self.y_field_shift = self.y_field_shift_default * self.scale_factor
        self.x_first_answer_shift = self.x_first_answer_shift_default * self.scale_factor
        self.y_first_answer_shift = self.y_first_answer_shift_default * self.scale_factor
        self.x_second_answer_shift = self.x_second_answer_shift_default * self.scale_factor
        self.y_second_answer_shift = self.y_second_answer_shift_default * self.scale_factor
        self.field_element_size = self.field_element_size_default * self.scale_factor

        self.playing_background_element = ActiveElement(self.x_field_shift + self.x_shift,
                                                        self.y_field_shift + self.y_shift,
                                                        self.field_element_size * self.field_number_elements,
                                                        self.field_element_size * self.field_number_elements)
        self.expression_field_element = ActiveElement(self.x_first_answer_shift + self.x_shift,
                                                      self.y_first_answer_shift + self.y_shift,
                                                      self.field_element_size * 1,
                                                      self.field_element_size * self.first_answer_number_elements)
        self.score_field_element = ActiveElement(self.x_second_answer_shift + self.x_shift,
                                                 self.y_second_answer_shift + self.y_shift)

    def update_cards(self):
        self.card_pic_width = self.card_pic_width_default * self.main_background.scale
        self.card_pic_height = self.card_pic_height_default * self.main_background.scale
        self.score_pic_width = self.score_pic_width_default * self.main_background.scale

        for card_value in self.list_cards_symbols:
            for card in self.card_sprites[card_value]:
                card.scale = self.main_background.scale
                if len(self.empty_card_sprites) != 0:
                    empty = self.empty_card_sprites.pop()
                    empty.x = card.x
                    empty.y = card.y
                    self.empty_card_sprites.append(empty)

        for empty in self.empty_card_sprites:
            empty.scale = self.main_background.scale

        for score_value in self.list_score_symbols:
            for key in self.score_sprites[score_value]:
                key.scale = self.main_background.scale

    def update_labels(self):
        self.label_score_x = self.label_score_x_default * self.main_background.scale
        self.label_speed_x = self.label_speed_x_default * self.main_background.scale
        self.label_level_x = self.label_level_x_default * self.main_background.scale
        self.label_y = self.label_y_default * self.main_background.scale

    def update_change_parameters(self):
        self.change_speed_x = self.change_speed_x_default * self.main_background.scale
        self.change_speed_y = self.change_speed_y_default * self.main_background.scale
        self.change_level_x = self.change_level_x_default * self.main_background.scale
        self.change_level_y = self.change_level_y_default * self.main_background.scale
        self.change_param_width = self.change_param_width_default * self.main_background.scale
        self.change_param_height = self.change_param_height_default * self.main_background.scale

        self.big_score_pic_width = self.big_score_pic_width_default * self.main_background.scale
        self.big_score_pic_height = self.big_score_pic_height_default * self.main_background.scale

        self.change_speed_element = ActiveElement(self.change_speed_x + self.x_shift,
                                                  self.change_speed_y + self.y_shift,
                                                  self.change_param_height, self.change_param_width)
        self.change_level_element = ActiveElement(self.change_level_x + self.x_shift,
                                                  self.change_level_y + self.y_shift,
                                                  self.change_param_height, self.change_param_width)

        for score_symbol in self.list_score_symbols:
            for card in self.big_score_sprites[score_symbol]:
                card.scale = self.main_background.scale * self.big_card_coef

    def update_menus(self):
        self.credits_sprite.scale = self.main_background.scale
        self.credits_sprite.x = self.main_background.x
        self.credits_sprite.y = self.main_background.y

        self.help_sprite.scale = self.main_background.scale
        self.help_sprite.x = self.main_background.x
        self.help_sprite.y = self.main_background.y

        self.credits_back_x = self.credits_back_x_def * self.scale_factor
        self.credits_back_y = self.credits_back_y_def * self.scale_factor
        self.credits_back_height = self.credits_back_height_def * self.scale_factor
        self.credits_back_width = self.credits_back_width_def * self.scale_factor
        self.credits_back = ActiveElement(self.credits_back_x + self.x_shift,
                                          self.credits_back_y + self.y_shift,
                                          self.credits_back_height, self.credits_back_width)

        # DIALOG
        self.dialog_sprite.scale = self.main_background.scale
        self.dialog_sprite.x = self.main_background.x
        self.dialog_sprite.y = self.main_background.y

        self.dialog_yes_x = self.dialog_yes_x_def * self.scale_factor
        self.dialog_yes_y = self.dialog_yes_y_def * self.scale_factor
        self.dialog_no_x = self.dialog_no_x_def * self.scale_factor
        self.dialog_no_y = self.dialog_no_y_def * self.scale_factor
        self.dialog_height = self.dialog_height_def * self.scale_factor
        self.dialog_width = self.dialog_width_def * self.scale_factor
        self.dialog_yes = ActiveElement(self.dialog_yes_x + self.x_shift,
                                        self.dialog_yes_y + self.y_shift,
                                        self.dialog_height, self.dialog_width)
        self.dialog_no = ActiveElement(self.dialog_no_x + self.x_shift,
                                       self.dialog_no_y + self.y_shift,
                                       self.dialog_height, self.dialog_width)

    def update_sliders(self):
        # self.music_volume, self.sounds_volume = self.mech.get_volumes()

        self.slider_height = self.slider_height_def * self.scale_factor
        self.slider_width = self.slider_width_def * self.scale_factor
        self.slider_first_x = self.slider_first_x_def * self.scale_factor
        self.slider_first_y = self.slider_first_y_def * self.scale_factor
        self.slider_second_x = self.slider_second_x_def * self.scale_factor
        self.slider_second_y = self.slider_second_y_def * self.scale_factor

        # Sounds
        self.slider_sprite_1.x = self.slider_first_x + (self.slider_second_x - self.slider_first_x) * \
            self.sounds_volume - self.slider_width / 2 + self.x_shift
        self.slider_sprite_1.y = self.slider_first_y - self.slider_height * 0.15 + self.y_shift
        self.slider_sprite_1.scale = self.main_background.scale
        self.slider_sprite_1.visible = self.show_sliders

        self.slider_area_1 = ActiveElement(self.slider_first_x + self.x_shift,
                                           self.slider_first_y + self.y_shift - self.slider_height / 5,
                                           self.slider_height / 1.5,
                                           self.slider_second_x - self.slider_first_x)

        # Music
        self.slider_sprite_2.x = self.slider_first_x + (self.slider_second_x - self.slider_first_x) * \
            self.music_volume - self.slider_width / 2 + self.x_shift
        self.slider_sprite_2.y = self.slider_second_y - self.slider_height * 0.15 + self.y_shift
        self.slider_sprite_2.scale = self.main_background.scale
        self.slider_sprite_2.visible = self.show_sliders

        self.slider_area_2 = ActiveElement(self.slider_first_x + self.x_shift,
                                           self.slider_second_y + self.y_shift - self.slider_height / 5,
                                           self.slider_height / 1.5,
                                           self.slider_second_x - self.slider_first_x)

    def update_active_game_features(self):
        self.trade_x = self.trade_x_def * self.scale_factor
        self.trade_y = self.trade_y_def * self.scale_factor
        self.trade_height = self.trade_height_def * self.scale_factor
        self.trade_width = self.trade_width_def * self.scale_factor
        self.add_extra_card_x = self.add_extra_card_x_def * self.scale_factor
        self.add_extra_card_y = self.add_extra_card_y_def * self.scale_factor
        self.add_extra_card_height = self.add_extra_card_height_def * self.scale_factor
        self.add_extra_card_width = self.add_extra_card_width_def * self.scale_factor

        self.trade = ActiveElement(self.trade_x + self.x_shift,
                                   self.trade_y + self.y_shift,
                                   self.trade_height, self.trade_width)

        self.add_extra_card = ActiveElement(self.add_extra_card_x + self.x_shift,
                                            self.add_extra_card_y + self.y_shift,
                                            self.add_extra_card_height, self.add_extra_card_width)

    # Flag of scalded window
    def is_window_scaled(self):
        if self.window.width == self.previous_window_sizes[0] and self.window.height == self.previous_window_sizes[1]:
            return False
        else:
            self.previous_window_sizes = (self.window.width, self.window.height)
            self.update_scaling_and_shifting()
            self.update_buttons()
            self.update_labels()
            self.update_fields()
            self.update_background()
            self.update_change_parameters()
            self.update_cards()
            return True
