import pyglet
import GUI
import graphics
import mechanics
import sounds
import time
import os
import gc
gc.disable()

pyglet.options['audio'] = ('directsound', 'pulse', 'openal', 'silent')
#pyglet.options['search_local_libs'] = True
#os.environ["PATH"] += "avbin"
#dll_name = os.path.join(os.path.dirname(__file__), 'avbin')
#pyglet.lib.load_library(dll_name)


class GameState:
    menu = 0
    game = 1
    pause = 2
    options = 3
    quit = 4
    help = 5


class Game:
    def __init__(self):
        self.game_state = GameState.menu
        self.framerate = 60
        self.active_dialog = False

        self.soun = sounds.SoundModule(self)
        self.mech = mechanics.Mechanics(self, self.soun)
        self.graph = graphics.Graphics(self.mech)
        self.gui = GUI.GUI(self.graph, self, self.mech, self.soun, GameState())

        self.need_to_reset = False
        self.end_of_game = False

        self.volume_music, self.volume_sounds = self.mech.get_volumes()

        self.audio_player = self.soun.player
        self.audio_player.queue(self.soun.main_playlist())
        self.audio_player.volume = self.volume_music  # громкость основной мелодии

        pyglet.clock.schedule_interval(self.update_game, 1.0 / self.framerate)
        pyglet.app.run()

    def update_state(self, new_state):
        self.soun.play_sound('menu_pick', self.volume_sounds)
        self.game_state = new_state

    def get_state(self):
        return self.game_state

    def update_game(self, dt):
        self.audio_player.play()
        if self.game_state == GameState.menu:
            self.graph.show_menu(dialog=self.active_dialog)
        elif self.game_state == GameState.options:
            self.graph.show_options()
        elif self.game_state == GameState.help:
            self.graph.show_options(help=True)
        elif self.game_state == GameState.game:
            if self.need_to_reset:
                self.mech.reset_game()
                self.need_to_reset = False
            self.mech.update()
            self.graph.show_game(dialog=self.active_dialog)
        elif self.game_state == GameState.pause:
            self.graph.show_game(dialog=self.active_dialog, paused=True and not self.end_of_game)
        elif self.game_state == GameState.quit:
            self.audio_player.next_source()
            time.sleep(0.5)
            pyglet.app.exit()

        self.update_volumes()
        self.graph.update_picture()

    def game_over(self):
        self.game_state = GameState.pause
        self.end_of_game = True

    def reset_game(self):
        self.need_to_reset = True

    def update_volumes(self):
        self.volume_music, self.volume_sounds = self.mech.get_volumes()
        self.audio_player.volume = self.volume_music

    def close_dialog(self):
        self.soun.play_sound('menu_pick', self.volume_sounds)
        self.active_dialog = False

    def open_dialog(self):
        self.soun.play_sound('menu_pick', self.volume_sounds)
        self.active_dialog = True


if __name__ == '__main__':
    Game()
