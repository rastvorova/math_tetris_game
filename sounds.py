import pyglet


class SoundModule:
    def __init__(self, game):
        self.game = game
        self.game_state = self.game.get_state()
        self.welcome_f = True

        self.sounds_path = 'sounds/'
        self.sounds = {}
        # Нужны еще звуки нажатия на кнопки в меню, переключение режимов
        for sound_name in ('main_theme', 'welcome_sound', 'complete_sound', 'pick', 'drop',
                           'error', 'quit', 'menu_pick', 'trade_in'):
            self.sounds[sound_name] = pyglet.media.load(self.sounds_path + '{}.wav'.format(sound_name), streaming=False)

        self.player_states = {0, 1, 3}
        self.player = pyglet.media.Player()
        
        self.player.queue(self.main_playlist())

    def play_sound(self, name, volume=0.5):
        self.sounds[name].play().volume = volume
        # self.sounds[name].play()

    def update_for_sound(self, state):
        self.game_state = state

    def main_playlist(self):
        self.game_state = self.game.get_state()
        while self.game_state in self.player_states:
            self.game_state = self.game.get_state()
            if self.game_state not in self.player_states:
                continue
            self.player.loop = True
            yield self.sounds['main_theme']

        yield self.sounds['quit']
