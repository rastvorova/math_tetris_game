import numpy as np
import random


class Mechanics:
    def __init__(self, game_obj, sound_module):
        self.game_obj = game_obj
        self.sound_module = sound_module
        self.field_width = 10
        self.field_height = 10
        self.playfield = PlayingField(self.field_width, self.field_height)
        # добавляем в механику строчку под формулку
        self.formulafield = FormulaField()

        # добавляем в механику карточку, которую мы мышкой перетаскиваем.
        # если мы ничего не перетаскиваем, там в поле card будет None.
        # а когда перетаскиваем, там будет соответствующая карточка.
        # никаких методов пока не имеет, но мы добавим по мере необходимости
        self.carried_card = None
        self.cycles_elapsed = 0
        self.framerate = 60
        self.speed = 0
        self.level = 0
        self.game_card_addition_speed_a = 0.1
        self.game_card_addition_speed_b = 0.3
        self.fall_card_speed = 3
        self.correct_answers = 0
        self.score = 0
        self.score_length = 7
        self.level_bind = {0: (0, 100), 1: (100, 400), 2: (400, 1000), 3: (1000, 2000), 4: (2000, 4000),
                           5: (4000, 10000), 6: (10000, 50000), 7: (50000, 100000),
                           8: (100000, 300000), 9: (300000, 999999)}
        self.start_speed = 0
        self.start_level = 0

        # Sounds and music volume
        self.carried_slider = None
        self.volume_music = 0.5
        self.volume_sounds = 0.5

    def get_volumes(self):
        return self.volume_music, self.volume_sounds

    def set_music_volume(self, music_volume):
        self.volume_music = music_volume

    def set_sounds_volume(self, sounds_volume):
        self.volume_sounds = sounds_volume

    def get_score(self):
        return self.score

    def get_speed(self):
        return self.speed

    def get_level(self):
        return self.level

    def update(self):
        if self.playfield.field_overflow():
            self.game_obj.game_over()
            return
        if self.cycles_elapsed % (self.framerate / self.fall_card_speed) == 0:
            self.playfield.grav_update()
        if self.cycles_elapsed % round(self.framerate / (self.game_card_addition_speed_a * self.speed +
                                                         self.game_card_addition_speed_b)) == 0:
            self.playfield.put_card_on_top()
        self.check_equality()
        self.cycles_elapsed += 1

    def add_extra_card_on_top(self):
        self.playfield.put_card_on_top()
        self.score += 1

    def check_equality(self):
        if self.formulafield.is_answer_field_empty():
            value = self.generate_answer()
            self.formulafield.set_answer_by_value(value)
        else:
            if self.formulafield.is_equality_true():
                self.sound_module.play_sound('complete_sound', self.volume_sounds)  # Проигрывание эффекта победы)
                self.update_score()
                self.formulafield.clear_formula_field()
                value = self.generate_answer()
                self.formulafield.set_answer_by_value(value)
                self.correct_answers += 1
                if self.correct_answers > 9:
                    self.correct_answers = 0
                    if self.level < 9:
                        self.level += 1

    def generate_answer(self):
        left, right = self.level_bind[self.level]
        return random.randint(left, right)

    def update_score(self):
        for i in self.formulafield.expression_array:
            if i is not None:
                if i.card_value in ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'open_bkt', 'close_bkt'):
                    self.score += 2
                elif i.card_value in ('plus', 'minus'):
                    self.score += 5
                elif i.card_value in ('multiply', 'divide'):
                    self.score += 7
        if self.score >= 9999999:
            self.score = 9999999

    def get_playfield(self):
        return self.playfield, self.field_width, self.field_height

    def get_formulafield(self):
        return self.formulafield, self.formulafield.formula_length, self.formulafield.answer_length

    def reset_game(self):
        self.formulafield.clear_answer_field()
        self.formulafield.clear_formula_field()
        self.score = 0
        self.playfield.pseudo_random = 0.5
        self.playfield.previous_value = None
        # self.level = self.level_setting # that after pause set level to 0. don't need
        self.correct_answers = 0
        self.playfield.reset()

    def get_card_from_field(self, i_field, j_field, x, y):
        card = self.playfield.get_card_by_indexes(i_field, j_field)

        if card is not None:
            self.carried_card = card
            self.carried_card.card_coord_shift(x, y)
            self.playfield.set_card_by_indexes(i_field, j_field, None)

            self.sound_module.play_sound('pick', self.volume_sounds)

    def get_card_from_formula(self, i_expr, x, y):
        card = self.formulafield.take_card(i_expr)
        if card is not None:
            self.carried_card = card
            self.carried_card.card_coord_shift(x, y)

            self.sound_module.play_sound('pick', self.volume_sounds)

    def put_card_to_field(self, i_field, j_field):
        if self.carried_card is not None:
            card = self.playfield.get_card_by_indexes(i_field, j_field)
            if card is None:
                self.playfield.set_card_by_indexes(i_field, j_field, self.carried_card)
            else:
                self.playfield.put_card_on_top(self.carried_card)
            self.carried_card = None

            self.sound_module.play_sound('drop', self.volume_sounds)

    def put_card_to_formula(self, i_expr):
        if self.carried_card is not None:
            card = self.formulafield.get_card_expression_by_index(i_expr)
            if card is not None:
                self.playfield.put_card_on_top(self.carried_card)
            else:
                self.formulafield.put_card(self.carried_card, i_expr)
            self.carried_card = None

            self.sound_module.play_sound('drop', self.volume_sounds)

    def get_carried_card(self):
        return self.carried_card

    def update_carried_card_coord(self, x, y):
        self.carried_card.card_coord_shift(x, y)

    def drop_card(self, traded=False):
        if self.carried_card is not None:
            if traded and self.score - 10 >= 0:
                self.playfield.put_card_on_top(self.carried_card, traded=traded)
                self.carried_card = None
                self.score -= 10

                self.sound_module.play_sound('trade_in', self.volume_sounds)  # ADD NEW SOUND
            else:
                self.playfield.put_card_on_top(self.carried_card)
                self.carried_card = None

                self.sound_module.play_sound('error', self.volume_sounds)

    def change_speed(self, reverse=False):
        self.sound_module.play_sound('pick', self.volume_sounds)

        if reverse:
            self.start_speed = self.start_speed - 1 if self.start_speed > 0 else 9
        else:
            self.start_speed = self.start_speed + 1 if self.start_speed < 9 else 0

    def change_level(self, reverse=False):
        self.sound_module.play_sound('pick', self.volume_sounds)

        if reverse:
            self.start_level = self.start_level - 1 if self.start_level > 0 else 9
        else:
            self.start_level = self.start_level + 1 if self.start_level < 9 else 0

    def update_speed_and_level(self, reset=False):
        if reset:
            self.start_speed = 0
            self.start_level = 0
            self.speed = 0
            self.level = 0
        else:
            self.speed = self.start_speed
            self.level = self.start_level


class PlayingField:
    def __init__(self, width, height):
        self.array = np.array([[None]*width]*height)  # все верно
        self.width = width
        self.height = height

        self.card_set_numbers = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
        self.card_set_operands = ('plus', 'minus', 'multiply', 'divide')
        self.card_set_brackets = ('open_bkt', 'close_bkt')

        self.previous_value = None
        self.probability_of_choose_brackets = 0.7
        self.pseudo_random = 0.5
        self.pseudo_num_oper_coef = 0.15

        # self.card_set = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        #                  'plus', 'minus', 'multiply', 'divide', 'open_bkt', 'close_bkt')
        self.bracket_counter = 0  # -1 - more open brackets, 1 - more close brakets

    def reset(self):
        for column in range(0, self.width):
            for row in range(0, self.height):
                self.array[column, row] = None

    def go_trade(self, traded_card=None):
        value = None
        if traded_card.card_value in self.card_set_numbers:
            value = random.choice(self.card_set_numbers)
        elif traded_card.card_value in self.card_set_operands:
            value = random.choice(self.card_set_operands)
        elif traded_card.card_value in self.card_set_brackets:
            value = random.choice(self.card_set_brackets)

        return value

    def field_overflow(self):
        elements_in_field = 0
        for column in range(0, self.width):
            elements_in_column = 0
            for row in range(0, self.height):
                if self.array[column, row] is not None:
                    elements_in_column += 1
            elements_in_field += elements_in_column
        if elements_in_field >= self.height * self.width:
            return True
        return False

    def put_card_on_top(self, card=None, traded=False):
        y_index = self.height-1
        x_index = None
        while x_index is None:
            rand_ind = np.random.randint(0, self.width)
            x_index = rand_ind if self.array[rand_ind, y_index] is None else None
        if card is not None:
            if traded:
                value = self.go_trade(card)
                new_card = Card(card_value=value, index_x=x_index, index_y=y_index)
            else:
                new_card = card
        else:
            # value = random.choice(self.card_set)
            if random.random() < self.pseudo_random:  # 0.5
                self.pseudo_random /= (1 + self.pseudo_random) * \
                                      (1 - self.pseudo_num_oper_coef)  # decrease probability of many numbers in a row
                value = random.choice(self.card_set_numbers)
                if value == self.previous_value:                       # decrease probability of double values in a row
                    value = random.choice(self.card_set_numbers)
            else:
                self.pseudo_random *= (1 + self.pseudo_random) * \
                                      (1 + self.pseudo_num_oper_coef)  # decrease probability of many operands in a row
                if random.random() < self.probability_of_choose_brackets:
                    value = random.choice(self.card_set_operands)
                    if value == self.previous_value:                   # decrease probability of double values in a row
                        value = random.choice(self.card_set_operands)
                else:
                    value = random.choice(self.card_set_brackets)

            self.previous_value = value
            if value in ('open_bkt', 'close_bkt'):
                if self.bracket_counter == -1:
                    value = 'close_bkt'
                    self.bracket_counter = 0
                elif self.bracket_counter == 1:
                    value = 'open_bkt'
                    self.bracket_counter = 0
                else:
                    self.bracket_counter = -1 if value == 'open_bkt' else 1
            new_card = Card(card_value=value, index_x=x_index, index_y=y_index)
        self.array[x_index, y_index] = new_card

        return True

    def get_card_by_indexes(self, x_ind, y_ind):
        return self.array[x_ind, y_ind]

    def set_card_by_indexes(self, x_ind, y_ind, value):
        if value is None or type(value) == Card:
            self.array[x_ind, y_ind] = value

    def grav_update(self):
        for row in range(0, self.height-1):
            for column in range(0, self.width):
                if self.array[column, row] is None and self.array[column, row+1] is not None:
                    self.array[column, row] = self.array[column, row+1]
                    self.array[column, row+1] = None


class Card:
    def __init__(self, card_value=None, is_on_field=None, index_x=None, index_y=None, coord_x=None, coord_y=None):
        self.card_value = card_value
        self.is_on_field = is_on_field
        self.card_index_x = index_x
        self.card_index_y = index_y
        self.card_x = coord_x
        self.card_y = coord_y

    def card_coord_shift(self, coord_x=None, coord_y=None):
        self.card_x = coord_x
        self.card_y = coord_y

    def card_index_shift(self, index_x=None, index_y=None):
        self.card_index_x = index_x
        self.card_index_y = index_y


class FormulaField:
    def __init__(self, formula_length=19, answer_length=7):
        self.formula_length = formula_length
        self.answer_length = answer_length
        self.expression_array = np.array([None] * formula_length)
        self.answer_array = np.array([None] * answer_length)

    def put_card(self, card, index):
        self.expression_array[index] = card

    def take_card(self, index):
        returned_card = self.expression_array[index]
        self.expression_array[index] = None
        return returned_card

    def get_card_expression_by_index(self, index):
        return self.expression_array[index]

    def clear_formula_field(self):
        for index in range(0, self.formula_length):
            self.expression_array[index] = None

    def clear_answer_field(self):
        for index in range(0, self.answer_length):
            self.answer_array[index] = None

    def get_card_answer_by_index(self, index):
        return self.answer_array[index]

    def is_answer_field_empty(self):
        for item in self.answer_array:
            if item is not None:
                return False
        return True

    def set_answer_by_value(self, value):
        self.clear_answer_field()
        # number = str(random.randint(1, 99))
        number = str(value)
        self.answer_array[0] = Card(card_value='equally')
        for i in range(0, len(number)):
            self.answer_array[i+1] = Card(card_value=number[i])
        # j = 0
        # for i in (str(number)):
        #    self.answer_array[j] = i
        #    j += 1

    def is_equality_true(self):
        symbols = {'plus': '+', 'minus': '-', 'multiply': '*', 'divide': '//',
                   'open_bkt': '(', 'close_bkt': ')'}
        expression_answer_temp = ''
        for i in self.expression_array:
            if i is not None:
                if i.card_value in ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'):
                    expression_answer_temp = expression_answer_temp + i.card_value
                # elif i.card_value == '0':
                #    if expression_answer_temp[-1] in ('1', '2', '3', '4', '5', '6', '7', '8', '9'):
                #        expression_answer_temp = expression_answer_temp + i.card_value
                #    else:
                #        continue
                elif i.card_value in ('plus', 'minus', 'multiply', 'divide', 'open_bkt', 'close_bkt'):
                    expression_answer_temp = expression_answer_temp + symbols[i.card_value]
        if expression_answer_temp == '':
            return False
        try:
            expression_answer = eval(expression_answer_temp)
        except Exception:
            return False
        # print("expr an: {}".format(expression_answer))
        answer_formula = ''
        for i in range(1, self.answer_length):
            if self.answer_array[i] is not None:
                answer_formula += str(self.answer_array[i].card_value)
        if answer_formula == '':
            return False
        # print("nas f: {}".format(answer_formula))
        return True if expression_answer == int(answer_formula) else False


class CarriedCard:
    def __init__(self):
        self.card = None

    def get_card(self):
        return self.card

    def set_card(self, card):
        self.card = card
