from cx_Freeze import setup, Executable
import sys
import subprocess

# ALSO NEED: collections, ctypes, encodings, importlib, numpy

packages = ["pyglet"]

includes = ['collections', 'ctypes', 'encodings', 'importlib', 'numpy']

installed_packages = subprocess.check_output([sys.executable, '-m', 'pip', 'freeze']).decode('utf-8')
installed_packages = installed_packages.split('\n')
EXCLUDES = {pkg.split('==')[0] for pkg in installed_packages if pkg != ''}
EXCLUDES.add('tkinter')

for item in packages:
    if item in EXCLUDES:
        EXCLUDES.remove(item)
for item in includes:
    if item in EXCLUDES:
        EXCLUDES.remove(item)


build_exe_options = {"packages": packages,
                     'includes': includes,
                     'excludes': EXCLUDES,
                     "include_files": [('sprites/resized', 'sprites/resized'), "sounds", "avbin.dll"],
                     'include_msvcr': True,
                     'optimize': '1'
                     }
base = None
if sys.platform == 'win32':
    base = 'Win32GUI'

executables = [Executable('game.py',
                          base=base,
                          targetName='Matesha',
                          icon='sprites/icon.ico')]

setup(name='Matesha',
      version='1.0',
      description='Matesha puzzle game',
      options={'build_exe': build_exe_options},
      executables=executables)

