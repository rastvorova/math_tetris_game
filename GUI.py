from pyglet.window import mouse


class GUI:
    def __init__(self, graphics_obj, game_obj, mechanics_obj, sound_obj, game_state):
        self.graphics_obj = graphics_obj
        self.mechanics = mechanics_obj
        self.window = graphics_obj.get_window()
        self.game_obj = game_obj
        self.sound_obj = sound_obj
        self.GameState = game_state

        self.dialog_exit = False
        self.dialog_start_new_game = False

        self.from_pause = False
        self.from_game = False
        self.from_menu = False

        self.make_handlers()

    def make_handlers(self):
        @self.window.event
        def on_mouse_press(x, y, button, modifiers):
            game_state = self.game_obj.game_state
            dialog = self.game_obj.active_dialog

            if button == mouse.LEFT:
                # DIALOG OPTIONS
                if dialog:
                    if self.dialog_exit:
                        if self.graphics_obj.dialog_yes.is_touched(x, y):
                            self.game_obj.update_state(self.GameState.quit)
                        elif self.graphics_obj.dialog_no.is_touched(x, y):
                            self.game_obj.close_dialog()
                            self.dialog_exit = False
                    elif self.dialog_start_new_game:
                        if self.graphics_obj.dialog_yes.is_touched(x, y):
                            self.game_obj.reset_game()
                            self.mechanics.update_speed_and_level()
                            self.game_obj.close_dialog()
                            self.game_obj.update_state(self.GameState.game)
                            self.dialog_start_new_game = False
                        elif self.graphics_obj.dialog_no.is_touched(x, y):
                            self.game_obj.close_dialog()
                            self.dialog_start_new_game = False
                else:
                    # GAME STATE OPTIONS
                    if game_state == self.GameState.menu:  # COMPLETE
                        if self.graphics_obj.menu_start_game.is_touched(x, y):
                            self.mechanics.update_speed_and_level()
                            self.game_obj.update_state(self.GameState.game)
                        elif self.graphics_obj.menu_pause.is_touched(x, y):
                            pass
                        elif self.graphics_obj.menu_options.is_touched(x, y):
                            self.game_obj.update_state(self.GameState.options)
                            self.graphics_obj.show_sliders = False
                            self.from_menu = True
                        elif self.graphics_obj.menu_quit.is_touched(x, y):
                            self.game_obj.open_dialog()
                            self.dialog_exit = True
                        elif self.graphics_obj.menu_help.is_touched(x, y):
                            self.game_obj.update_state(self.GameState.help)
                            self.graphics_obj.show_sliders = False
                            self.from_menu = True
                    elif game_state == self.GameState.game:
                        if self.graphics_obj.menu_start_game.is_touched(x, y):
                            self.game_obj.open_dialog()
                            self.dialog_start_new_game = True
                        elif self.graphics_obj.menu_pause.is_touched(x, y):
                            self.game_obj.update_state(self.GameState.pause)
                        elif self.graphics_obj.menu_options.is_touched(x, y):
                            self.game_obj.update_state(self.GameState.pause)
                            self.game_obj.update_state(self.GameState.options)
                            self.graphics_obj.show_sliders = False
                            self.from_game = True
                        elif self.graphics_obj.menu_help.is_touched(x, y):
                            self.game_obj.update_state(self.GameState.pause)
                            self.game_obj.update_state(self.GameState.help)
                            self.graphics_obj.show_sliders = False
                            self.from_game = True
                        elif self.graphics_obj.menu_quit.is_touched(x, y):
                            self.game_obj.open_dialog()
                            self.dialog_exit = True

                        # GAME ACTIONS
                        if self.graphics_obj.playing_background_element.is_touched(x, y):
                            i_field, j_field = self.graphics_obj.get_playing_field_index(x, y)
                            self.mechanics.get_card_from_field(i_field, j_field, x, y)
                        elif self.graphics_obj.expression_field_element.is_touched(x, y):
                            i_expr = self.graphics_obj.get_expression_field_index(x)
                            self.mechanics.get_card_from_formula(i_expr, x, y)
                        elif self.graphics_obj.add_extra_card.is_touched(x, y):
                            self.mechanics.add_extra_card_on_top()
                    elif game_state == self.GameState.pause:
                        if self.graphics_obj.menu_start_game.is_touched(x, y):
                            self.game_obj.open_dialog()
                            self.dialog_start_new_game = True
                        elif self.graphics_obj.menu_pause.is_touched(x, y):
                            self.game_obj.update_state(self.GameState.game)
                        elif self.graphics_obj.menu_options.is_touched(x, y):
                            self.game_obj.update_state(self.GameState.options)
                            self.graphics_obj.show_sliders = False
                            self.from_pause = True
                        elif self.graphics_obj.menu_help.is_touched(x, y):
                            self.game_obj.update_state(self.GameState.help)
                            self.graphics_obj.show_sliders = False
                            self.from_pause = True
                        elif self.graphics_obj.menu_quit.is_touched(x, y):
                            self.game_obj.open_dialog()
                            self.dialog_exit = True
                    elif game_state == self.GameState.options or game_state == self.GameState.help:
                        if self.graphics_obj.credits_back.is_touched(x, y):
                            self.graphics_obj.show_sliders = True
                            if self.from_pause:
                                self.game_obj.update_state(self.GameState.pause)
                                self.from_pause = False
                            elif self.from_game:
                                self.game_obj.update_state(self.GameState.game)
                                self.from_game = False
                            elif self.from_menu:
                                self.game_obj.update_state(self.GameState.menu)
                                self.from_menu = False

            # SOUNDS AND CHANGES OPTIONS
            if button == mouse.LEFT and self.graphics_obj.show_sliders:
                # SOUND OPTIONS
                if self.graphics_obj.slider_area_1.is_touched(x, y):
                    self.graphics_obj.set_volume(x, music=False)
                elif self.graphics_obj.slider_area_2.is_touched(x, y):
                    self.graphics_obj.set_volume(x, music=True)
                # CHANGE SPEED AND LEVEL PRESET
                if not dialog:
                    if self.graphics_obj.change_speed_element.is_touched(x, y):
                        self.mechanics.change_speed(reverse=False)
                    elif self.graphics_obj.change_level_element.is_touched(x, y):
                        self.mechanics.change_level(reverse=False)
            # UNDO
            elif button == mouse.RIGHT and self.graphics_obj.show_sliders:
                # SOUND OPTIONS
                if self.graphics_obj.slider_area_1.is_touched(x, y):
                    self.graphics_obj.set_volume(x, music=False, reset=True)
                elif self.graphics_obj.slider_area_2.is_touched(x, y):
                    self.graphics_obj.set_volume(x, music=True, reset=True)
                # CHANGE SPEED AND LEVEL PRESET
                if not dialog:
                    if self.graphics_obj.change_speed_element.is_touched(x, y):
                        self.mechanics.change_speed(reverse=True)
                    elif self.graphics_obj.change_level_element.is_touched(x, y):
                        self.mechanics.change_level(reverse=True)

        @self.window.event
        def on_mouse_release(x, y, buttons, modifiers):  # координаты отпускания мыши
            game_state = self.game_obj.game_state
            if buttons == mouse.LEFT and game_state == self.GameState.game:
                # GAME ACTIONS
                if self.graphics_obj.playing_background_element.is_touched(x, y):
                    i_field, j_field = self.graphics_obj.get_playing_field_index(x, y)
                    self.mechanics.put_card_to_field(i_field, j_field)
                elif self.graphics_obj.expression_field_element.is_touched(x, y):
                    i_expr = self.graphics_obj.get_expression_field_index(x)
                    self.mechanics.put_card_to_formula(i_expr)
                elif self.graphics_obj.trade.is_touched(x, y):
                    self.mechanics.drop_card(traded=True)
                else:
                    self.mechanics.drop_card()

        @self.window.event
        def on_mouse_drag(x, y, dx, dy, buttons, modifiers):
            # GAME ACTIONS
            if buttons & mouse.LEFT and self.mechanics.get_carried_card() is not None:
                self.mechanics.update_carried_card_coord(x, y)

            # SOUNDS ACTIONS
            elif buttons & mouse.LEFT and self.graphics_obj.slider_area_1.is_touched(x, y) \
                    and self.graphics_obj. show_sliders:
                self.graphics_obj.set_volume(x, music=False)
            elif buttons & mouse.LEFT and self.graphics_obj.slider_area_2.is_touched(x, y) \
                    and self.graphics_obj.show_sliders:
                self.graphics_obj.set_volume(x, music=True)
